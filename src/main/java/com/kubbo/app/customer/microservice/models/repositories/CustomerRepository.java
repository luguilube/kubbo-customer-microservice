package com.kubbo.app.customer.microservice.models.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.kubbo.app.customer.microservice.models.entities.Customer;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
	public List<Customer> findAllByDeletedFalse();
	public Page<Customer> findAllByDeletedFalse(Pageable pageable);
	public Optional<Customer> findById(Long id);
}
