package com.kubbo.app.customer.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class KubboCustomerMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubboCustomerMicroserviceApplication.class, args);
	}

}
