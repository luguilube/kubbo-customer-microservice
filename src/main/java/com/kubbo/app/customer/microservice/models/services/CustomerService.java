package com.kubbo.app.customer.microservice.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.kubbo.app.customer.microservice.models.entities.Customer;

public interface CustomerService {
	public List<Customer> findAllByDeletedFalse();
	public Page<Customer> findAllByDeletedFalse(Pageable pageable);
	public Customer findById(Long id);
	public Customer save(Customer customer);
	public boolean deleteById(Long id);
}
