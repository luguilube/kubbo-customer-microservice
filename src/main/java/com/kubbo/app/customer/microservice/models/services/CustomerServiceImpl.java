package com.kubbo.app.customer.microservice.models.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kubbo.app.customer.microservice.models.entities.Customer;
import com.kubbo.app.customer.microservice.models.repositories.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Customer> findAllByDeletedFalse() {
		return repository.findAllByDeletedFalse();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Customer> findAllByDeletedFalse(Pageable pageable) {
		return repository.findAllByDeletedFalse(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Customer findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Customer save(Customer customer) {
		return repository.save(customer);
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {
		Optional<Customer> op = repository.findById(id);
		
		if (!op.isEmpty()) {
			op.get().setDeleted(true);
			repository.save(op.get());
			
			return true;
		}
		
		return false;
	}

}
