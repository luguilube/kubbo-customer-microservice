package com.kubbo.app.customer.microservice.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kubbo.app.customer.microservice.models.entities.Customer;
import com.kubbo.app.customer.microservice.models.services.CustomerService;

@RestController
public class CustomerController {
	private static final Logger log = LoggerFactory.getLogger(CustomerController.class);
	private final CustomerService service;
	
	@Autowired
	public CustomerController(CustomerService service) {
		this.service = service;
	}
	
	@GetMapping("/list")
	public ResponseEntity<?> list() {
		try {
            return ResponseEntity.ok().body(service.findAllByDeletedFalse());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
	}
	
	@GetMapping("/paginated-list")
    public ResponseEntity<?> paginatedList(Pageable pageable) {
        try {
            return ResponseEntity.ok().body(service.findAllByDeletedFalse(pageable));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @PostMapping("store")
    public ResponseEntity<?> store(@Valid @RequestBody Customer customer, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return validation(result);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(customer));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> get(@PathVariable Long id) {
        try {
            Customer customer = service.findById(id);

            if (customer == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok().body(customer);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }
    
    @PutMapping("update/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody Customer customer, BindingResult result, @PathVariable Long id) {
        try {
            if (result.hasErrors()) {
                return validation(result);
            }
            
            Customer customerToUpdate = service.findById(id);
            
            if (customerToUpdate == null) {
            	return ResponseEntity.notFound().build();
            }
            
            customerToUpdate.setName(customer.getName());
            customerToUpdate.setSurname(customer.getSurname());
            customerToUpdate.setEmail(customer.getEmail());
            customerToUpdate.setBirthdate(customer.getBirthdate());
            customerToUpdate.setPhone(customer.getPhone());
            customerToUpdate.setAddress(customer.getAddress());
            
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(customerToUpdate));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }


    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
        	return service.deleteById(id) 
            		? ResponseEntity.noContent().build() 
    				: ResponseEntity.notFound().build();
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    private ResponseEntity<?> validation(BindingResult result) {
        Map<String, Object> errors = new HashMap<>();

        result.getFieldErrors().forEach(err -> {
            errors.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
        });

        return ResponseEntity.badRequest().body(errors);
    }

}
