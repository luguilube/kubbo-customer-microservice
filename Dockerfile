FROM openjdk:11

VOLUME /tmp

ADD ./target/kubbo-customer-microservice-0.0.1-SNAPSHOT.jar kubbo-customer-microservice.jar

ENTRYPOINT ["java", "-jar", "/kubbo-customer-microservice.jar"]